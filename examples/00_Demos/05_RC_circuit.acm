/* 
   Works with semantic 2014
   Works with semantic 2013
   Works with semantic 2012
*/

// Battery charging and discharging
//
// Author:  Anita Sant'Anna, 2012
//
// Run this example, view Plot, and then 3D

// This example models a simple RC circuit 
// described by the differential equation: 
//
//    Vc'=(Vin-Vc)/(R*C);
//      where:
//       Vc: voltage over capacitor
//       Vin: input voltage
//       R: resistance
//       C: capacitance

class CircuitRC(C,R,POS)
  private 
    Vc:=0; Vc':=0; Vin:=5;  
    _3D:=["Cylinder",POS,[0.1,0],[0,1,0],[0,0,0]]; 
  end
  
  Vc'=(Vin-Vc)/(R*C);
  _3D=["Cylinder",POS,[0.1,Vc/2],[0,1,0],[3.1416/2,0,0]];
end


class Main(simulator)
  private 
    // Create two RC circuits with different parameters
    circuit1 := create  CircuitRC(0.02,10,[0,0,0]); 
    circuit2 := create  CircuitRC(0.2,10,[1,0,0]); 
    t:=0; t':=1; mode:="charge";
  end
  t'=1;
  // The input voltage to the circuits 
  //   will alternate between 5V and 0V every second
  switch mode
    case "charge"
      circuit1.Vin=5;
      circuit2.Vin=5;
      if t >= 1
        mode := "discharge";
      end
    case "discharge"
      circuit1.Vin=0;
      circuit2.Vin=0;
      if t>=2
        t:=0; 
        mode := "charge";
      end 
  end
end

