/* 
   Has error with semantic 2014
   Has error with semantic 2013
   Works with semantic 2012
*/

// comparison.acm
// Authors:  Walid, Roland, Yingfu

// 1. Geometry and Visualization

// 1.1. The _3D variable for display

#semantics "2012 Reference"

class display_bar (v,c,D)
 private 
   _3D := [];  
  flag := 0
 end
 if (flag == 1)
   terminate self
 end;
 _3D := ["Cylinder", D+[0,0.2,v/2],
        [0.02,v],c,
        [-1*pi/2,0,0]];
end

class sphere (m,D)
 private 
  p :=[0,0,0];
  _3D := [];   // Please replace all _3D := initializations by []
  flag := 0;
 end
 if (flag == 1)
   terminate self
 end;
 _3D = [["Sphere", D+p,
           0.03*sqrt(m),
           [m/3,2+sin(m),2-m/2],
           [1,1,1]]];
end

class cylinder (D)
 private 
  p :=[0,0,0]; q:=[0,0,0];
  _3D := [];
  radius := 0.01;
  length := 0.01; alpha:=0; theta:= pi/2;
  x:=0;y:=0;z:=0;
  flag := 0;
 end
 if (flag == 1)
   terminate self;
 end;
 _3D = [["Cylinder",(p+q)/2+D,[radius,length],[1,1,1],
                      [alpha,0,-theta]]];
 x = dot(p-q,[1,0,0]);
 y = dot(p-q,[0,1,0]);
 z = dot(p-q,[0,0,1]);

  length = norm(p-q);
  alpha = asin(z/length);

  if (y>0)
   theta = asin(x/(length*cos(alpha)))
  else
   theta = -asin(x/(length*cos(alpha)))
              +pi
  end
end

// 2.1 Basic dynamics. F:=ma, energy, 

class mass_1d (m,p0,D)
 private p:=p0; p':=0; p'':=0; f:=0; e_k:=0;
         s:=create sphere (m,D);
  flag := 0;
 end
 if (flag == 1)
   s.flag := 1;
   terminate self;
 end;
 p'' = f/m;
 e_k = 0.5 * m * (p') ^2;
 s.p = [0,0,p]
end
class spring_1d (k,l,D)
 private p1:=0; p2:=0; f1:=0; f2:=0; e_p:=0;
         c := create cylinder (D);
         flag := 0;
 end
 if (flag == 1)
   c.flag := 1;
   terminate self
 end;
 f1  =  k*(p2-p1+l);
 f2  = -k*(p2-p1+l);
 e_p = 0.5 * k * (p2-p1+l)^2;
 c.p = [0,0,p1];
 c.q = [0,0,p2];
end

// 2.2 Gravity (falling mass)

class example_1 (D)
 private m := create mass_1d (10,3,D);
         flag := 0;
 end
 if (flag == 1)
   m.flag := 1;
   terminate self
 end;
 m.f = m.m * -9.8;
end

class example_2 (D) // Mass-Spring-Mass (MSM)
 private m1 := create mass_1d (15, 1,D);
         m2 := create mass_1d ( 5,-1,D);
         s  := create spring_1d (5,1.25,D);
         flag := 0;
 end
 if (flag == 1)
   m1.flag := 1;
   m2.flag := 1;
   s.flag := 1;
   terminate self
 end;
 s.p1 = m1.p;
 s.p2 = m2.p;
 m1.f = s.f1;
 m2.f = s.f2;
end

class example_3 (D) // 3 Mass - 2 Spring (MS32)
 private m1 := create mass_1d (15,1,D);
         m2 := create mass_1d (5,-1,D);
         m3 := create mass_1d (1,-1.5,D);
         s1 := create spring_1d (5,1.75,D);
         s2 := create spring_1d (5,0.5,D);
         b  := create display_bar 
               (0,[0.1,3,0.1],D);
         flag := 0;
 end
 if (flag == 1)
   m1.flag := 1;
   m2.flag := 1;
   m3.flag := 1;
   s1.flag := 1;
   s2.flag := 1;
   b.flag := 1;
   terminate self
 end;
 s1.p1 = m1.p;
 s1.p2 = m2.p;
 s2.p1 = m2.p;
 s2.p2 = m3.p;
 m1.f  = s1.f1;
 m2.f  = s1.f2 + s2.f1;
 m3.f  = s2.f2;
 b.v   = (m1.e_k + m2.e_k + m3.e_k + s1.e_p + s2.e_p)*12;
end

class controller_p1 (k_p)
 private g:=0; v:=0; f:=0 end
 f = k_p * (g-v)
end

class controller_pd1 (k_p,k_d)
 private g:=0; v:=0; s:=0; f:=0 end
 f = k_p * (g-v) - k_d*s
end

class example_4 (D) // Control (CPMS32)
private  m1 := create mass_1d (15,1,D);
         m2 := create mass_1d (5,-1,D);
         m3 := create mass_1d (1,-1.5,D);
         s1 := create spring_1d (5,1.75,D);
         s2 := create spring_1d (5,0.5,D);
         c  := create controller_p1 (1);
         b  := create display_bar 
               (0,[0.1,3,0.1],D);
         flag := 0;
end
if (flag == 1)
   m1.flag := 1;
   m2.flag := 1;
   m3.flag := 1;
   s1.flag := 1;
   s2.flag := 1;
   b.flag := 1;
   terminate self
end;

 s1.p1 = m1.p;
 s1.p2 = m2.p;

 s2.p1 = m2.p;
 s2.p2 = m3.p;

 c.g = s1.l+s2.l;   // goal is spring length at rest
 c.v = m1.p-m3.p;   // value is actual spring length
 
 m1.f = s1.f1 + c.f;   // Note c.f
 m2.f = s1.f2 + s2.f1;
 m3.f = s2.f2 - c.f;   // Note c.f

 b.v = (m1.e_k + m2.e_k + m3.e_k + s1.e_p + s2.e_p)*12;

end

class example_5 (D) // Control (CPDMS32)
private  m1 := create mass_1d (15,1,D);
         m2 := create mass_1d (5,-1,D);
         m3 := create mass_1d (1,-1.5,D);
         s1 := create spring_1d (5,1.75,D);
         s2 := create spring_1d (5,0.5,D);
         c  := create controller_pd1 (1,1);
         b  := create display_bar 
               (0,[0.1,3,0.1],D);
         flag := 0;
 end
 if (flag == 1)
   m1.flag := 1;
   m2.flag := 1;
   m3.flag := 1;
   s1.flag := 1;
   s2.flag := 1;
   b.flag := 1;
   terminate self
end;


 s1.p1 = m1.p;
 s1.p2 = m2.p;

 s2.p1 = m2.p;
 s2.p2 = m3.p;

 c.g = s1.l+s2.l;   // goal is spring length at rest
 c.v = m1.p-m3.p;   // value is actual spring length
 c.s = m1.p'-m3.p'; // speed is difference in speeds
 
 m1.f = s1.f1 + c.f;   // Note c.f
 m2.f = s1.f2 + s2.f1;
 m3.f = s2.f2 - c.f;   // Note c.f

 b.v = (m1.e_k + m2.e_k + m3.e_k + s1.e_p + s2.e_p)*12;

end

class example_6 (D) // Side-by-side MS32, CPMS32, CPDMS32
 private example3 := create example_3 (D+[-0.5,0,0]);
         example4 := create example_4 (D+[   0,0,0]);
         example5 := create example_5 (D+[ 0.5,0,0]);
         flag := 0;
 end
 if(flag == 1)
   example3.flag := 1;
   example4.flag := 1;
   example5.flag := 1;
   terminate self;
 end;

end


// 5.  Dynamics and Control in 3D
//     5.1 3D Mass

class mass (m,p0,D)
 private p:=p0; p':=[0,0,0]; p'':=[0,0,0]; f:=[0,0,0]; e_k:=0;
         s := create sphere (m,D);
         flag := 0; 
 end
 if(flag == 1)
   s.flag := 1;
   terminate self;
 end;
 p'' = f/m;
 e_k = 0.5 * m * (dot(p',p')) ^2;
 s.p = p;
end

// 5.2 3D Spring
// Added visualization for the spring
class spring (k,l0,D)
 private p1:=[0,0,0]; p2:=[0,0,0]; f1:=[0,0,0]; f2:=[0,0,0];
         dl := [0,0,0]; e_p:=0;
         s := create cylinder(D);
         flag := 0;
 end
 if(flag == 1)
   s.flag := 1;
   terminate self;
 end;
 dl  = p2-p1 * (1-l0/norm(p2-p1));
 f1  =  k*dl;
 f2  = -k*dl;
 e_p = 0.5 * k * dot(dl,dl);
 s.p = p1; s.q = p2;
end


// 5.3 PD and PID Control in 3D

class controller_pd (k_p,k_d)
 private g:=[0,0,0]; v:=[0,0,0]; s:=[0,0,0]; f:=[0,0,0] end
 f = k_p * (g-v) - k_d*s
end

class controller_pid (k_p,k_i,k_d)
 private g:=[0,0,0]; v:=[0,0,0]; s:=[0,0,0]; f:=[0,0,0];
         i:=[0,0,0]; i':=[0,0,0] end
 f  = k_p * (g-v) + k_i*i - k_d*s;  // We might write as k_d*(g‚Äô - v‚Äô)
 i' = (g-v)                                // We might define error instead of g -v 
end

// 6. Disturbance (autonomous)

class disturbance (k)
 private t:=0; t':=0; t'':=0; f:=[0,0,0] end // No need for t‚Äô‚Äô 
 t' = 4;
 f  = k*[sin(t), cos(t), sin(2*t+cos(3*t))]
end

class mass_on_leash (m,p0,D)
 private p:=p0; p':=[0,0,0]; p'':=[0,0,0]; 
         f:=[0,0,0];
         s := create sphere (m,D);
         c := create cylinder (D);
         flag := 0;
 end
 if(flag == 1)
   s.flag := 1;
   c.flag := 1;
   terminate self;
 end;
 p'' = f/m;
 s.p = p; c.p = p; c.q = [0,0,0];
end

// Examples

// NOTE: if timeStep is 0.01, the system is not stable!!
// Once changed to 0.001, the system become stable.
class example_7 (D) // Double spring pendulum (SP2)
 private m1 := create mass   (1.9,[0,0,-0.5],D);
         m2 := create mass   (1.1,[0,0.5,-1],D);
         s1 := create spring (10,0.5,D);
         s2 := create spring (20,0.05,D);
         flag := 0;
 end
 if(flag == 1)
   m1.flag := 1;
   m2.flag := 1;
   s1.flag := 1;
   s2.flag := 1;
   terminate self;
 end;
 s1.p1 = [0,0,1];
 s1.p2 = m1.p;
 s2.p1 = m1.p;
 s2.p2 = m2.p;

 m1.f = s1.f2 + s2.f1 + [0,0,-9.8];
 m2.f = s2.f2 + [0,0,-9.8];
end

class example_8 (D) // Controller PD for Mass (CPDM)
 private m := create mass (1,[1,1,1],D);
         c := create controller_pd (15,3);
         flag := 0;
 end
  if(flag == 1)
   m.flag := 1;
   terminate self;
 end;

 c.g = [0,0,1];
 c.v = m.p;
 c.s = m.p';
 m.f = c.f + [0,0,-9.8]*m.m;
end
class example_9 (D) // Controller PID for Mass (CPDM)
 private m := create mass (1,[1,1,1],D);
         c := create controller_pid (15,10,3);
         flag := 0;
 end
 if(flag == 1)
   m.flag := 1;
   terminate self;
 end;

 c.g = [0,0,1];
 c.v = m.p;
 c.s = m.p';
 m.f = c.f + [0,0,-9.8];
end

class example_10 (k_p,k_i,k_d,D) 
 // CPDM+PID+disturbance (CPIDM+D)
 private g := create mass (0.5, [0,0,1],D);
         r := create mass_on_leash (4,[-0.3,0.3,0.9],D);
         c := create controller_pid (k_p,k_i,k_d);
         d := create disturbance (1);
         flag := 0;
 end
 if(flag == 1)
   g.flag := 1;
   r.flag := 1;
   terminate self;
 end;

 c.g = g.p;
 c.v = r.p;
 c.s = r.p';
 r.f = c.f + [0,0,-9.8] + d.f;
end
class example_11 (D) // Varying control params
 private e1 := create example_10 (25,8,6, D+[ 1, 0,0]);
         e2 := create example_10 (25,0,6, D+[ 0.5,0,0]);
         e3 := create example_10 (25,8,3, D+[ 0,0,0]);
         e4 := create example_10 ( 5,8,6, D+[-0.5, 0,0]);
         e5 := create example_10 ( 5,0,2, D+[-1, 0,0]);
         flag := 0;
 end
  if(flag == 1)
   e1.flag := 1;
   e2.flag := 1;
   e3.flag := 1;
   e4.flag := 1;
   e5.flag := 1;
   terminate self;
 end;

end
class example_12 (k_p,k_i,k_d,D)
 // CPDM+PID+disturbance (CPIDM+D)
 private g := create mass (0.5, [0,0,1],D);
         r := create mass (4,[0,0,0],D);
         c := create controller_pid (k_p,k_i,k_d);
         d := create disturbance (1);
         flag := 0;
 end
  if(flag == 1)
   g.flag := 1;
   r.flag := 1;
   terminate self;
 end;

 c.g   = g.p;
 c.v   = r.p;
 c.s   = r.p';
 r.f   = c.f + [0,0,-9.8] + d.f;
end

class dumbbell (mp,mq,D)
 private 
  p  :=[0,0,1]; q:=[0,0,-1];
  c  := create cylinder (D);
  s1 := create sphere (mp,D);
  s2 := create sphere (mq,D);
  flag := 0;
 end
 if(flag == 1)
   c.flag := 1;
   s1.flag := 1;
   s2.flag := 1;
   terminate self;
 end;
 c.p  = p;  c.q = q;
 s1.p = p; s2.p = q;

end
// 7. Basic rigid body dynamics and control
class rod (m,l,c0,a0,b0,D)
 private 
   c := c0; c' := [0,0,0];
   c'' := [0,0,0];
   a:=a0;a':=0;a'':=0;
   b:=b0;b':=0;b'':=0;
   c1 := create dumbbell (1,1,D);
   g := 9.81;
   torque := [0,0,0];
   fp := [0,0,0];
   fq := [0,0,0];
   p := [0,0,0];
   q := [0,0,0];

   vp := [0,0,0];
   vq := [0,0,0];
   flag := 0;
 end
 if(flag == 1)
   c1.flag := 1;
   terminate self;
 end;
  torque = cross(fp,l/2*[cos(a)*cos(b),sin(a)*cos(b),sin(a)]) -
           cross(fq,l/2*[cos(a)*cos(b),sin(a)*cos(b),sin(a)]);
  
  c'' = (fp + fq)/m - g * [0,0,1];
  a'' = dot(torque,[0,1,0])/(m*l^2) - l/2* b'^2 * cos(a)*sin(a);
  b'' = dot(torque,[0,0,1])/(m*l^2) -l/2 * b'*a'*cos(a)*sin(a)/cos(a)^2;
 
  p = c - [cos(a)*cos(b),cos(a)*sin(b),sin(a)]*l/2;
  q = c + [cos(a)*cos(b),cos(a)*sin(b),sin(a)]*l/2;


  vq =  c'+ cross(l/2 * [0,a',b'],[cos(a)*cos(b),sin(a)*cos(b),sin(a)]);
  vp =  c' - cross(l/2 * [0,a',b'],[cos(a)*cos(b),sin(a)*cos(b),sin(a)]);

  c1.p = p;
  c1.q = q;

end

class example_13 (k_p,k_i,k_d,D)
  private g := create mass (0.1, [0,0,0],D);
         r  := create rod (2,1,[0.5,0,0],0,0,D);
         cp := create controller_pid (k_p,k_i,k_d);
         cq := create controller_pid (k_p, k_i, k_d);
         t := 0; t' := 1; 
         flag := 0;  
 end
 if(flag == 1)
   g.flag := 1;
   r.flag := 1;
   terminate self;
 end;
  t' = 1;
 
  cp.g   = [0,0,0];
  cp.v   = r.p;
  cp.s   = r.vp;

  
  cq.g   = [0,0,1];
  cq.v   = r.q;
  cq.s   = r.vq;
  r.fq = cq.f;
  r.fp  = cp.f ;

end

// Acumen model of a QuadCopter system
class Display_bar1 (v,c,D)
 private
  roll := 0;
  pitch := 0;
  // Arrow
  _3D := [["Cylinder", D, 
         [0.02,v], c, 
         [-3.14159265359/2,0,0]],
         ["Cone", D, 
         [0.02,v], c, 
         [-3.14159265359/2,0,0]]
         ]
 end
 c = [3,3,0];
 _3D = [
        ["Cylinder", D+[-sin(pitch)*v/2,0,-v/2-0.1],
        [0.05*v+0.02,v],c,
        [-3.14159265359/2,-roll-pitch,0]],
        ["Cone", D+[0,0,-0.1],
        [0.2*v+0.05,0.2*v+0.05],c,
        [pi/2,-roll-pitch,0]]
        
        ];
end

// Dynamic of the QuadCopter system
class QuadCopter(GlobalP,phi,theta,psi)
  private
    // Parameters
    g := 9.81;  // m/s2
    m := 0.468; // kg
    l := 0.225; // m
    k := 2.98*10^(-6);
    b := 1.140*10^(-7);
    IM := 3.357*10^(-5); // kg m2
    Ixx := 4.856*10^(-3);// kg m2
    Iyy := 4.856*10^(-3);// kg m2
    Izz := 8.801*10^(-3);// kg m2
    Ax  := 0.25;  // kg/s
    Ay  := 0.25;  // kg/s
    Az  := 0.25;  // kg/s
    // Rotor angular velocity
    w1 := 0; w2:= 0; w3 := 0; w4 := 0; // rad/s
    wT := 0;
    // Force fi
    f1 := 0; f2:= 0; f3 := 0; f4 := 0;
    // Torque TMi
    TM1 := 0; TM2 := 0; TM3 := 0; TM4 := 0;
    T := 0; //
    // Coordinates
      // Global position 
    GlobalP' := [0,0,0]; GlobalP'' := [0,0,0];
    // Global orientation
    //phi := 0.1; theta := 0.1; psi := 0.1;
    phi' := 0; theta' := 0; psi' := 0;
    phi'' := 0; theta'' := 0; psi'' := 0; 
    LocalV := [0,0,0];    // Local linear velocity
    p := 0; q := 0; r := 0;  // Local angular velocity
    p' := 0; q' := 0; r' := 0; 
    time := 0; time' := 1;
    hoverW := 620;  // Stable rotor velocity 


  end
    time' = 1;
    T = k* (w1^2 + w2^2 + w3^2 + w4^2);
    f1 = k * w1^2; TM1 = b * w1^2;
    f2 = k * w2^2; TM2 = b * w2^2;
    f3 = k * w3^2; TM3 = b * w3^2;
    f4 = k * w4^2; TM4 = b * w4^2;
    // Newton-Euler equations
    GlobalP'' = -g * [0,0,1] + T/m * [cos(psi)*sin(theta)*cos(phi) + sin(psi)*sin(phi),
                                      sin(psi)*sin(theta)*cos(phi) - cos(psi)*sin(phi),
                                      cos(theta)*cos(phi)]
                 -1/m*[Ax*dot(GlobalP',[1,0,0]),Ay*dot(GlobalP',[0,1,0]),Az*dot(GlobalP',[0,0,1]) ]; 
                 // Aerodynamical effect
    wT = w1 - w2 + w3 - w4;
    p' = (Iyy - Izz)*q*r/Ixx - IM*q/Ixx*wT + l*k*(-1* w2^2 + w4^2)/Ixx;
    q' = (Izz - Ixx)*p*r/Iyy - IM*(-p)/Iyy*wT + l*k*(-1*w1^2 +w3^2)/Iyy;
    // TODO: The meaning of this one?
    r' = (Ixx - Iyy)*p*q/Izz  + b*(w1^2 +w2^2 -w3^2 -w4^2)/Izz;
    // Global orientational equation

    phi''   = 0 * p + (phi'*cos(phi)*tan(theta)+theta'*sin(phi)/cos(theta)^2)*q + 
                      (-phi'*sin(phi)*cos(theta)+theta'*cos(phi)/cos(theta)^2)*r +
                      (p'+q'*sin(phi)*tan(theta)+r'*cos(phi)*tan(theta));
    theta'' = 0 * p + (-phi'*sin(phi))*q + (-phi'*cos(phi))*r +
                      (q'*cos(phi)+r'*(-sin(phi)));
    psi''   = 0 * p + (phi'*cos(phi)/cos(theta)+phi'*sin(phi)*tan(theta)/cos(theta))*q +
                      (-phi'*sin(phi)/cos(theta)+theta'*cos(phi)*tan(theta)/cos(theta))*r+
                      (q'*sin(phi)/cos(theta)+r'*cos(phi)/cos(theta));

end

// Objective is drive the quadcopter to hover position(all angle to zero)
class PDController(z,phi,theta,psi,zp,phip,thetap,psip)
  private
    // Parameters
    zd := 0; phid := 0; thetad := 0; psid := 0;
    errorZ := 0; errorZP := 0;
    errorPhi := 0; errorPhiP := 0;
    errorTheta := 0; errorThetaP := 0; 
    errorPsi := 0; errorPsiP := 0; 
    Kzd := 2.5; Kzp := 1.5;
    Kpd := 1.75;Kpp := 6;
    Ktd := 1.75;Ktp := 6;
    Kpsd := 1.75;Kpsp := 6;
    Ixx := 4.856*10^(-3);// kg m2
    Iyy := 4.856*10^(-3);// kg m2
    Izz := 8.801*10^(-3);// kg m2
    g := 9.81;  // m/s2
    m := 0.468; // kg
    l := 0.225; // m
    k := 2.98*10^(-6);
    b := 1.140*10^(-7);
    T := 0;
    torqueP := 0; torqueT := 0; torquePsi := 0;
    w1 := 0; w2 := 0; w3 := 0; w4 := 0; // Output control signal
  end
    errorZ = zd - z;
    errorPhi = phid - phi;
    errorTheta = thetad - theta;
    //errorPsi  = psid - psi;

    errorZP = 0 - zp;
    errorPhiP = 0 - phip;
    errorThetaP = 0 - thetap;
    //errorPsiP  = 0 - psip;
    
    T = (g + Kzd*(errorZP) + Kzp*(errorZ))*m/(cos(phi)*cos(theta));
    torqueP = (Kpd*(errorPhiP)+Kpp*(errorPhi))*Ixx;
    torqueT = (Ktd*(errorThetaP)+Ktp*(errorTheta))*Iyy;
    torquePsi = (Kpsd*(errorPsiP)+Kpsp*(errorPsi))*Izz;
    
    w1 = sqrt(abs(T/(4*k) - torqueT/(2*k*l) - torquePsi/(4*b)));
    w2 = sqrt(abs(T/(4*k) - torqueP/(2*k*l) + torquePsi/(4*b)));
    w3 = sqrt(abs(T/(4*k) + torqueT/(2*k*l) - torquePsi/(4*b)));
    w4 = sqrt(abs(T/(4*k) + torqueP/(2*k*l) + torquePsi/(4*b)));
    
end

class VisualCopter(p,roll,pitch,yaw) // Global position and orientation
  private
    l := 2;
    rotor4p := [0,1,0];
    rotor2p := [0,-1,0];
    rotor1p := [1,0,0];
    rotor3p := [-1,0,0];
    _3D := []
  end
     rotor4p = p+[l/2 * cos(roll) * sin(yaw), l/2 * cos(roll) * cos(yaw),l/2 * sin(roll)];
     rotor2p = p-[l/2 * cos(roll) * sin(yaw), l/2 * cos(roll) * cos(yaw),l/2 * sin(roll)];
     rotor3p = p+[-1*l/2 * cos(pitch) * cos(yaw), l/2 * cos(pitch) * sin(yaw),l/2 * sin(pitch)];
     rotor1p = p-[-1*l/2 * cos(pitch) * cos(yaw), l/2 * cos(pitch) * sin(yaw),l/2 * sin(pitch)];
     _3D = [
            ["Cylinder",rotor1p, [0.3,0.04],[0.5,0.5,0.5],[pi/2+roll,-pitch,0]] , // Rotor1
            ["Cylinder",rotor3p, [0.3,0.04],[0.5,0.5,0.5],[pi/2+roll,-pitch,0]] , // Rotor3
            ["Cylinder",rotor2p, [0.3,0.04],[0.5,0.5,0.5],[pi/2+roll,-pitch,0]] , // Rotor2
            ["Cylinder",rotor4p, [0.3,0.04],[0.5,0.5,0.5],[pi/2+roll,-pitch,0]] , // Rotor4
            ["Cylinder",p,[0.02,2],[1,0,0],[roll,0,-yaw] ],  //Local y-axis
            ["Cylinder",p,[0.02,2],[0,0,1],[pitch,0,-yaw+pi/2] ]  // Local x-axis
           ]
end
class example_14()
  private
    mode := "Init";
    copter := create QuadCopter([2.2,0,1],0,-0.5,0);
    visual := create VisualCopter([2.2,0,1],0,-0.5,0);
    f1 := create Display_bar1(0,[0.1,0.8,0.1],[1,0,0]);
    f2 := create Display_bar1(0,[0.1,0.8,0.1],[0,-1,0]);
    f3 := create Display_bar1(0,[0.1,0.8,0.1],[-1,0,0]);
    f4 := create Display_bar1(0,[0.1,0.8,0.1],[0,1,0]);
    // Controller
    controller := create PDController(1,0.2,0.2,0.2,0,0,0,0);
    flag := 0;
    max := 0;
  end
  f2.roll = visual.roll;
  f4.roll = visual.roll;
  f1.roll = visual.roll;
  f3.roll = visual.roll;
  f2.pitch = visual.pitch;
  f4.pitch = visual.pitch;
  f1.pitch = visual.pitch;
  f3.pitch = visual.pitch;
  
  f1.v = (copter.w1-max)*30/max;
  f1.D = visual.rotor1p;
  f2.v = (copter.w2-max)*30/max;
  f2.D = visual.rotor2p;
  f3.v = (copter.w3-max)*30/max;
  f3.D = visual.rotor3p;
  f4.v = (copter.w4-max)*30/max;
  f4.D = visual.rotor4p;


  max := copter.w1;
  if(max>copter.w2)
    max := copter.w2;
  end;
  if(max>copter.w3)
    max := copter.w3;
  end;
  if(max>copter.w4)
    max := copter.w4;
  end;
  
  
  switch mode
    case "Init"
      //simulator.endTime := 6.0;
      //simulator.timeStep := 0.001;

      controller.z = dot(copter.GlobalP, [0,0,1]);
      controller.zp = dot(copter.GlobalP', [0,0,1]);
      controller.phi = copter.phi;
      controller.theta = copter.theta;
      controller.psi = copter.psi;
      controller.phip = copter.phi';
      controller.thetap = copter.theta';
      controller.psip = copter.psi';  
      //controller.timeStep = simulator.timeStep;    
  
      copter.w1 = controller.w1; 
      copter.w2 = controller.w2;   
      copter.w3 = controller.w3;   
      copter.w4 = controller.w4;      

      visual.p = copter.GlobalP;
      visual.roll = copter.phi;
      visual.pitch = copter.theta;
      visual.yaw = copter.psi;
      
     
    case "Persist"
  end
end


class Main(simulator)
 private
          e := 0;
          t := 0; t' := 0;
          mode := "init";
          example := 1;
          exampleTime := 5;
          caption := "";
          _3D := [];
 end
 _3D = [["Text",[-1.5,0.2,-2],0.5,[1.6,1.6,0.5],[pi/2-0.3,0,-0.3],example],
        ["Text",[-0.5,0,-2],0.3,[1.6,1.6,0.1],[pi/2-0.3,0,-0.3],caption]];
 t' = 1;
 switch mode
  case "init"
   mode := example;
  case 1
   e := create example_1([0,0,0]);
   mode := "1Continue"
  case "1Continue"
   caption = "Falling Ball";
   if (t > exampleTime)
    mode := "terminate"
   end;
  case 2
   e := create example_2([0,0,0]);
   mode := "2Continue"
  case "2Continue"
   caption = "2Mass 1Spring";
   if (t > exampleTime)
    mode := "terminate"
   end;
  case 3
   e := create example_3([0,0,0]);
   mode := "3Continue"
  case "3Continue"
   caption = "3Mass 2Spring";
   if (t > exampleTime)
    mode := "terminate"
   end;
  case 4
   e := create example_4([0,0,0]);
   mode := "4Continue"
  case "4Continue"
   caption = "P Control";
   if (t > exampleTime)
    mode := "terminate"
   end;
  case 5
   e := create example_5([0,0,0]);
   mode := "5Continue"
  case "5Continue"
   caption = "PD Control";
   if (t > exampleTime)
    mode := "terminate"
   end;
  case 6
   e := create example_6([0,0,0]);
   mode := "6Continue"
  case "6Continue"
   caption = "None P PD";
   if (t > exampleTime)
    mode := "terminate"
   end;
  case 7
   e := create example_7([0,0,0]);
   mode := "7Continue"
  case "7Continue"
   caption = "2 Spring-Pendulum";
   if (t > exampleTime)
    mode := "terminate"
   end;
  case 8
   e := create example_8([0,0,0]);
   mode := "8Continue"
  case "8Continue"
   caption = "PD for Mass";
   if (t > exampleTime)
    mode := "terminate"
   end;
 case 9
   e := create example_9([0,0,0]);
   mode := "9Continue"
  case "9Continue"
   caption = "PID for Mass";
   if (t > exampleTime)
    mode := "terminate"
   end;
 case 10
   e := create example_10(40,10,10,[0,0,0]);
   mode := "10Continue"
  case "10Continue"
   caption = "PID for Mass-Rod";
   if (t > exampleTime)
    mode := "terminate"
   end;
 case 11
   e := create example_11([0,0,0]);
   mode := "11Continue"
  case "11Continue"
   caption = "Vary PID parameters";
   if (t > exampleTime)
    mode := "terminate"
   end;
 case 12
   e := create example_12(25,8,3,[0,0,0]);
   mode := "12Continue"
  case "12Continue"
   caption = "PID with disturbance";
   if (t > exampleTime)
    mode := "terminate"
   end;
 case 13
   e := create example_13(20,2,8,[0,0,0]);
   mode := "13Continue"
  case "13Continue"
   caption = "PID for 3D-rod";
   if (t > exampleTime)
    mode := "terminate"
   end;
 case 14
   e := create example_14();
   mode := "14Continue"
  case "14Continue"
   caption = "PID for Quad-Copter";
   if (t > exampleTime)
    mode := "terminate"
   end;

  case "terminate"
    e.flag := 1;
    mode := "rest"
  case "rest"
    mode := "init";
    example := example + 1;
    t := 0;
 end;
 simulator.endTime := 70;
end

