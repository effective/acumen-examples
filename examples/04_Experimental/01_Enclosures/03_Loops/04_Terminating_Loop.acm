
// Terminating Loop Example
//
// Author:  Jan Duracz
// Note:    Run using Semantics -> Enclosure

class Main(simulator)
private 
  mode := "one"; 
  x    := [0.0..1.0] // Non-constant reset 
end
  switch mode
    case "one"
      if true
        mode := "one";
        x := x/2
      end
  end;
  simulator.endTime := 2.0;
  simulator.minLocalizationStep := 1.0
end

// Note:  This model is an example of a looping 
//        system with a non-constant reset, where the
//        reachability analysis can terminate because 
//        the initial state contains all reachable 
//        states.
//
// Note:  The set of all reachable states is
//        { 0.5^k | k >= 0 }. Because the initial 
//        condition contains this set, the 
//        reachability analysis does terminate.
